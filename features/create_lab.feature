Feature: Create a Lab

  Background:
    Given Marketplace Main page is opened
    When I click 'Login' on Marketplace Main page
      Then Login page is opened
    When I login as 'ADMIN' user
      Then Home page is opened
      And User should be logged in
    When I open 'Assay Management' tools
    And I select last tab
      Then Assay Management page is opened

  @ui @CreateALab
  Scenario Outline: <test count> Possibility to successfully create an <Lab type>
    When I click 'Create a Lab' on Assay Management page
      Then Create a Lab page is opened
    When I fill following fields on Create a Lab page and save as 'lab':
      | Name     | TestLab    |
      | Lab type | <Lab type> |
    And I click 'Next' on Create a Lab page
      Then Lab Address page is opened
    When I fill following fields on Lab Address page and save as 'lab':
      | Address 1     | Test Address 1   |
      | Address 2     | Test Address 2   |
      | City / Town   | Test City        |
      | Region        | vr               |
      | Country       | Albania          |
      | Postal code   | Test Postal code |
    And I click 'Finish' on Lab Address page
      Then Lab Profile page is opened
      And 'Lab created.' message is displayed on Lab Profile page

    Examples:
      | Lab type       | test count |
      | Unspecified    | 0001       |
      | Academic Lab   | 0002       |
      | Commercial Lab | 0003       |
      | Hospital Lab   | 0004       |
