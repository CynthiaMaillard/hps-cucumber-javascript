Feature: Support internationalisation
    As a polyglot coffee lover
    I can select the language on the coffee machine
    So I can practice my use of greetings in several languages

  Scenario: No messages are displayed when machine is shut down
    Given the coffee machine is started
    When I shutdown the coffee machine
    Then message "" should be displayed

  Scenario Outline: Messages are based on language
    # Well, sometimes, you just get a coffee.
    When I start the coffee machine using language "<language>"
    When I add it on a table:
    | Name	| language	|
    | langue	| <language>	|
    Then message "<ready_message>" should be displayed
    And that's it

    Examples:
      | Lab type      | test count  |
      |Unspecified    | 0001        |
      |Academic Lab   | 0002        |

